export const sum = (firstNumber, secondNumber) => firstNumber + secondNumber;
export const sub = (firstNumber, secondNumber) => firstNumber - secondNumber;
export const mult = (firstNumber, secondNumber) => firstNumber * secondNumber;
export const div = (firstNumber, secondNumber) => Number.parseFloat(secondNumber / firstNumber).toPrecision(3);;
export const sqrt = (number) => Number.parseFloat(Math.sqrt(number)).toPrecision(3);
export const sumatory = (array) => array.reduce((accumulator, currentValue) => accumulator +=  currentValue)