export const selectCurrentNumber = (state) => {
  return state.currentNumber;
};

export const selectCurrentStack = (state) => {
  return state.stack;
};
