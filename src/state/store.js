import { createStore } from 'redux';

import { rootReducer } from './reducer';

const initialize = {
  stack: [],
  currentNumber: 0
}
export const store = createStore(rootReducer, initialize);
