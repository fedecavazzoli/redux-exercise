import * as Operations from 'operations'

export const pressNumber = (number) => ({
  type: 'INPUT',
  payload: number
});

export const pressSum = () => ({
  type: 'SUM'
})

export const pressIntro = () => ({
  type: 'INTRO'
})

export const pressSubstract = () => ({
  type: 'SUBSTRACT'
})

export const pressMultiplication = () => ({
  type: 'MULTIPLICATION'
})

export const pressDivision = () => ({
  type: 'DIVISION'
})

export const pressSqrt = () => ({
  type: 'SQRT'
})

export const pressSumatory = () => ({
  type: 'SUMATORY'
})
