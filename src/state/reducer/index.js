import * as Operations from 'operations'

export const rootReducer = (state, action) => {
  if (state === undefined) {
    return {};
  }

  switch (action.type) {
    case 'INPUT':
      if(!state.currentNumber)
        return{ ...state, currentNumber: action.payload };
      return { ...state, currentNumber: state.currentNumber * 10 + action.payload }

    case 'INTRO':
      if(state.currentNumber === 0)
        return{
          ...state
        };
      if(state.stack.length === 0)
        return {
          ...state,
          stack: state.stack.concat(state.currentNumber),
          currentNumber: 0
        }
      return {
        ...state,
        stack: state.stack.concat(state.currentNumber),
        currentNumber: 0
      }

    case 'SUM':
      if (state.stack.length < 2)
        return{
          ...state
        };
      var secondNumber = state.stack.pop();
      var firstNumber = state.stack.pop();
      return {
        ...state,
        stack: state.stack.concat(Operations.sum(firstNumber, secondNumber)),
        currentNumber: 0
      };
    
    case 'SUBSTRACT':
      if (state.stack.length < 2)
        return{
          ...state
        };
      var secondNumber = state.stack.pop();
      var firstNumber = state.stack.pop();
    return {
      ...state,
      stack: state.stack.concat(Operations.sub(firstNumber, secondNumber)),
      currentNumber: 0
    }; 

    case 'MULTIPLICATION':
      if (state.stack.length < 2)
        return{
          ...state
        };
      var secondNumber = state.stack.pop();
      var firstNumber = state.stack.pop();
      return {
        ...state,
        stack: state.stack.concat(Operations.mult(firstNumber, secondNumber)),
        currentNumber: 0
      };
    
    case 'DIVISION':
      if (state.stack.length < 2)
      return{
        ...state
      };
      var secondNumber = state.stack.pop();
      if(secondNumber === 0)
        return{
          ...state
        };

      var firstNumber = state.stack.pop();
      return {
        ...state,
        stack: state.stack.concat(Operations.div(firstNumber, secondNumber)),
        currentNumber: 0
      };
    
    case 'SQRT':
      if (state.stack.length < 1)
        return{
          ...state
        };
      var number = state.stack.pop();
      if(number < 0){
        state.stack.concat(number)
        return{
          ...state
        };
      }
      return {
        ...state,
        stack: state.stack.concat(Operations.sqrt(number)),
        currentNumber: 0
      };
      
    case 'SUMATORY':
      if (state.stack.length < 2)
        return{
          ...state
        };
      console.log(Operations.sumatory(state.stack))
      return {
        ...state,
        stack: [Operations.sumatory(state.stack)],
        currentNumber: 0
      }; 
    default:
      return state;
  }
};
