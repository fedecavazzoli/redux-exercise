import { useDispatch, useSelector } from 'react-redux';

import * as Actions from 'state/actions';
import { selectCurrentNumber, selectCurrentStack } from 'state/selectors';

import styles from './Calculator.module.css';

const renderStackItem = (value, index) => {
  return <div key={index}>{value}</div>;
};

export const Calculator = () => {
  const currentNumber = useSelector(selectCurrentNumber);
  const stack = useSelector(selectCurrentStack);

  const dispatch = useDispatch();
  const onClickNumber = (number) => {
    const action = Actions.pressNumber(number);
    dispatch(action);
  };
  const onClick = (fn) => {
    const action = fn;
    dispatch(action);
  };

  return (
    <div className={styles.main}>
      <div className={styles.display}>{currentNumber}</div>
      <div className={styles.numberKeyContainer}>
        {[...Array(9).keys()].map((i) => (
          <button key={i} onClick={() => onClickNumber(i + 1)}>
            {i + 1}
          </button>
        ))}
        <button className={styles.zeroNumber} onClick={() => onClickNumber(0)}>
          0
        </button>
        <button onClick={() => onClick()}>.</button>
      </div>
      <div className={styles.opKeyContainer}>
        <button onClick={() => onClick(Actions.pressSum())}>+</button>
        <button onClick={() => onClick(Actions.pressSubstract())}>-</button>
        <button onClick={() => onClick(Actions.pressMultiplication())}>x</button>
        <button onClick={() => onClick(Actions.pressDivision())}>/</button>
        <button onClick={() => onClick(Actions.pressSqrt())}>√</button>
        <button onClick={() => onClick(Actions.pressSumatory())}>Σ</button>
        <button onClick={() => onClick()}>Undo</button>
        <button onClick={() => onClick(Actions.pressIntro())}>Intro</button>
      </div>
      <div className={styles.stack}>{stack.map(renderStackItem)}</div>
    </div>
  );
};
